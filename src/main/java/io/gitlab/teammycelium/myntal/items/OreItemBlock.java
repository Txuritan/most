package io.gitlab.teammycelium.myntal.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;

public class OreItemBlock extends ItemBlock {
    public OreItemBlock(Block block) {
        super(block);
    }

    @Override
    public int getMetadata(int meta) {
        return meta;
    }
}

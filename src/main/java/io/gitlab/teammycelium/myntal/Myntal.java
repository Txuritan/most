package io.gitlab.teammycelium.myntal;

import com.google.gson.*;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import io.gitlab.teammycelium.myntal.blocks.OreBlock;
import io.gitlab.teammycelium.myntal.items.OreItemBlock;
import io.gitlab.teammycelium.myntal.templates.TemplateAlloy;
import io.gitlab.teammycelium.myntal.templates.TemplateElement;
import io.gitlab.teammycelium.myntal.templates.TemplateElementTypes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Objects;

@Mod(modid = "myntal", version = "${version}")
public class Myntal {
    public static final Logger LOGGER = LogManager.getLogger("Myntal");

    private String[] overlayTextures = new String[] {
            "ore_iron",
            "ore_lapis",
            "ore_emerald",
            "ore_quartz",
    };

    private ArrayList<TemplateAlloy> alloys = new ArrayList<>();
    private ArrayList<TemplateElement> elements = new ArrayList<>();

    ArrayList<OreBlock> oreBlocks = new ArrayList<>();

    @Mod.Instance("myntal")
    static Myntal INSTANCE;

    @SidedProxy(
            clientSide = "io.gitlab.teammycelium.myntal.ClientProxy",
            serverSide = "io.gitlab.teammycelium.myntal.CommonProxy"
    )
    static CommonProxy PROXY;

    public Myntal() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/assets/myntal/data.json")))) {
            JsonElement root = new JsonParser().parse(reader);

            if (root.isJsonArray()) {
                JsonArray nodes = root.getAsJsonArray();

                for (JsonElement element : nodes) {
                    if (element.isJsonObject()) {
                        JsonObject object = element.getAsJsonObject();
                        JsonElement templateElement = object.get("template");

                        if (Objects.nonNull(templateElement)) {
                            if (templateElement.isJsonPrimitive()) {
                                JsonPrimitive primitive = templateElement.getAsJsonPrimitive();

                                if (primitive.isString()) {
                                    switch (primitive.getAsString()) {
                                        case "alloy": {
                                            break;
                                        }
                                        case "crystalElement": {
                                            TemplateElement template = new TemplateElement().from(
                                                    TemplateElementTypes.CRYSTAL,
                                                    object
                                            );

                                            if (Objects.nonNull(template)) {
                                                elements.add(template);
                                            }

                                            break;
                                        }
                                        case "ingotElement": {
                                            TemplateElement template = new TemplateElement().from(
                                                    TemplateElementTypes.INGOT,
                                                    object
                                            );

                                            if (Objects.nonNull(template)) {
                                                elements.add(template);
                                            }

                                            break;
                                        }
                                    }
                                } else {
                                    LOGGER.error("`data.json` internal elements must have a `template` with the type string");
                                }
                            }
                        } else {
                            LOGGER.error("`data.json` internal elements must have a `template` string");
                        }
                    } else {
                        LOGGER.error("Internal `data.json` elements must be objects");
                    }
                }
            } else {
                LOGGER.error("`data.json`'s root element must be an array");
            }
        } catch (IOException io) {
            LOGGER.error("Unable to read from `data.json`", io);
        } catch (JsonIOException jsonIo) {
            LOGGER.error("Unable to read from BufferedReader", jsonIo);
        } catch (JsonSyntaxException jsonSyntax) {
            LOGGER.error("`data.json` is not valid and cannot be parsed", jsonSyntax);
        } catch (IllegalStateException illState) {
            LOGGER.error("Something tried to be something its not", illState);
        }
    }

    @SuppressWarnings("unused")
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        PROXY.init();

        for (TemplateElement element : elements) {
            String base;

            if (element.getGeneration().isEnd()) {
                base = "end_stone";
            } else if (element.getGeneration().isNether()) {
                base = "netherrack";
            } else {
                base = "stone";
            }

            OreBlock block = new OreBlock(
                    element.getName() + "_ore",
                    base,
                    overlayTextures[element.getOre().getTexture()],
                    new Color(Integer.decode(element.getColor())),
                    element.getOre().getHardness(),
                    element.getOre().getResistance()
            );

            GameRegistry.registerBlock(block, OreItemBlock.class, element.getName() + "_ore");

            oreBlocks.add(block);
        }
    }

    @SuppressWarnings("unused")
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {

    }

    @SuppressWarnings("unused")
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        PROXY.registerRenderers();
    }
}

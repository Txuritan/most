package io.gitlab.teammycelium.myntal;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;
import java.util.Objects;

public class JsonHelper {
    public static ArrayList<Integer> getArrayInteger(JsonObject object, String key) {
        ArrayList<Integer> integers = new ArrayList<>();

        JsonElement element = object.get(key);

        if (Objects.nonNull(element)) {
            if (element.isJsonArray()) {
                JsonArray array = element.getAsJsonArray();

                for (JsonElement arrayElement : array) {
                    if (Objects.nonNull(arrayElement)) {
                        if (arrayElement.isJsonPrimitive()) {
                            JsonPrimitive primitive = arrayElement.getAsJsonPrimitive();

                            if (primitive.isNumber()) {
                                integers.add(primitive.getAsInt());
                            } else {
                                Myntal.LOGGER.error("`" + key + "` must be a number");

                                return null;
                            }
                        } else {
                            Myntal.LOGGER.error("`" + key + "` elements must be a primitive");

                            return null;
                        }
                    } else {
                        Myntal.LOGGER.error("`" + key + "` elements must not be null");

                        return null;
                    }
                }
            } else {
                Myntal.LOGGER.error("`" + key + "` must be an array");

                return null;
            }
        } else {
            Myntal.LOGGER.error("`" + key + "` must not be null");

            return null;
        }

        return integers;
    }

    public static ArrayList<String> getArrayString(JsonObject object, String key) {
        ArrayList<String> strings = new ArrayList<>();

        JsonElement element = object.get(key);

        if (Objects.nonNull(element)) {
            if (element.isJsonArray()) {
                JsonArray array = element.getAsJsonArray();

                for (JsonElement arrayElement : array) {
                    if (Objects.nonNull(arrayElement)) {
                        if (arrayElement.isJsonPrimitive()) {
                            JsonPrimitive primitive = arrayElement.getAsJsonPrimitive();

                            if (primitive.isString()) {
                                strings.add(primitive.getAsString());
                            } else {
                                Myntal.LOGGER.error("`" + key + "` must be a string");

                                return null;
                            }
                        } else {
                            Myntal.LOGGER.error("`" + key + "` elements must be a primitive");

                            return null;
                        }
                    } else {
                        Myntal.LOGGER.error("`" + key + "` elements must not be null");

                        return null;
                    }
                }
            } else {
                Myntal.LOGGER.error("`" + key + "` must be an array");

                return null;
            }
        } else {
            Myntal.LOGGER.error("`" + key + "` must not be null");

            return null;
        }

        return strings;
    }

    public static Boolean getBoolean(JsonObject object, String key) {
        JsonElement element = object.get(key);

        if (Objects.nonNull(element)) {
            if (element.isJsonPrimitive()) {
                JsonPrimitive primitive = element.getAsJsonPrimitive();

                if (primitive.isBoolean()) {
                    return primitive.getAsBoolean();
                } else {
                    Myntal.LOGGER.error("`" + key + "` must be a float");

                    return null;
                }
            } else {
                Myntal.LOGGER.error("`" + key + "` must be a primitive");

                return null;
            }
        } else {
            Myntal.LOGGER.error("`" + key + "` must not be null");

            return null;
        }
    }

    public static JsonObject getObject(JsonObject object, String key) {
        JsonElement element = object.get(key);

        if (Objects.nonNull(element)) {
            if (element.isJsonObject()) {
                return element.getAsJsonObject();
            } else {
                Myntal.LOGGER.error("`" + key + "` must be a object");

                return null;
            }
        } else {
            Myntal.LOGGER.error("`" + key + "` must not be null");

            return null;
        }
    }

    public static Float getFloat(JsonObject object, String key) {
        JsonElement element = object.get(key);

        if (Objects.nonNull(element)) {
            if (element.isJsonPrimitive()) {
                JsonPrimitive primitive = element.getAsJsonPrimitive();
                if (primitive.isNumber()) {
                    return primitive.getAsFloat();
                } else {
                    Myntal.LOGGER.error("`" + key + "` must be a float");

                    return null;
                }
            } else {
                Myntal.LOGGER.error("`" + key + "` must be a primitive");

                return null;
            }
        } else {
            Myntal.LOGGER.error("`" + key + "` must not be null");

            return null;
        }
    }

    public static Integer getInt(JsonObject object, String key) {
        JsonElement element = object.get(key);

        if (Objects.nonNull(element)) {
            if (element.isJsonPrimitive()) {
                JsonPrimitive primitive = element.getAsJsonPrimitive();
                if (primitive.isNumber()) {
                    return primitive.getAsInt();
                } else {
                    Myntal.LOGGER.error("`" + key + "` must be a float");

                    return null;
                }
            } else {
                Myntal.LOGGER.error("`" + key + "` must be a primitive");

                return null;
            }
        } else {
            Myntal.LOGGER.error("`" + key + "` must not be null");

            return null;
        }
    }

    public static String getString(JsonObject object, String key) {
        JsonElement element = object.get(key);

        if (Objects.nonNull(element)) {
            if (element.isJsonPrimitive()) {
                JsonPrimitive primitive = element.getAsJsonPrimitive();
                if (primitive.isString()) {
                    return primitive.getAsString();
                } else {
                    Myntal.LOGGER.error("`" + key + "` must be a string");

                    return null;
                }
            } else {
                Myntal.LOGGER.error("`" + key + "` must be a primitive");

                return null;
            }
        } else {
            Myntal.LOGGER.error("`" + key + "` must not be null");

            return null;
        }
    }
}

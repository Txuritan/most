package io.gitlab.teammycelium.myntal.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.gitlab.teammycelium.myntal.TextureAtlasDynamic;
import io.gitlab.teammycelium.myntal.items.OreItemBlock;
import lombok.Setter;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

import java.awt.*;

public class OreBlock extends Block {
    @SideOnly(Side.CLIENT)
    private IIcon icon;

    private String base;
    private String overlay;
    private Color color;

    public int renderID = 0;

    public OreBlock(String name, String base, String overlay, Color color, Float hardness, Float resistance) {
        super(Material.rock);

        this.base = base;
        this.overlay = overlay;
        this.color = color;

        setBlockName(name);
        setHardness(hardness);
        setResistance(resistance);
        setCreativeTab(CreativeTabs.tabBlock);
        setBlockTextureName(base);
    }

    public int getRenderType() {
        return renderID;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(IBlockAccess world, int x, int y, int z, int side) {
        return icon;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return icon;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister iIconRegister) {
        if (iIconRegister instanceof TextureMap) {
            TextureMap textureMap = (TextureMap) iIconRegister;

            String registryName = "myntal:" + this.getUnlocalizedName();

            textureMap.setTextureEntry(registryName, new TextureAtlasDynamic(registryName, base, overlay, color));
            icon = textureMap.getTextureExtry(registryName);
        }
    }

    @Override
    public boolean isNormalCube(IBlockAccess world, int x, int y, int z) {
        return true;
    }
}

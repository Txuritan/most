package io.gitlab.teammycelium.myntal.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class WholeBlock extends Block {
    protected WholeBlock(String name, Float hardness, Float resistance) {
        super(Material.iron);

        setBlockName(name);
        setHardness(hardness);
        setResistance(resistance);

        GameRegistry.registerBlock(this, name);
    }
}

package io.gitlab.teammycelium.myntal.templates;

import com.google.gson.JsonObject;
import io.gitlab.teammycelium.myntal.JsonHelper;
import lombok.Getter;

import java.util.Objects;

public class TemplateArmor {
    @Getter
    private boolean enableAll = true;

    @Getter
    private Boolean enableHead = true;
    @Getter
    private Boolean enableChest = true;
    @Getter
    private Boolean enableLegs = true;
    @Getter
    private Boolean enableFeet = true;

    @Getter
    private TemplateArmorMaterial material;

    public TemplateArmor from(JsonObject object) {
        JsonObject parametersObject = JsonHelper.getObject(object, "parameters");
        if (Objects.nonNull(parametersObject)) {
            JsonObject materialObject = JsonHelper.getObject(parametersObject, "material");
            if (Objects.nonNull(materialObject)) {
                TemplateArmorMaterial templateArmorMaterial = new TemplateArmorMaterial().from(materialObject);
                if (Objects.nonNull(templateArmorMaterial)) {
                    material = templateArmorMaterial;
                } else {
                    return null;
                }
            } else {
                return null;
            }

            Boolean enableAllBoolean = JsonHelper.getBoolean(parametersObject, "enableAll");
            if (Objects.nonNull(enableAllBoolean)) {
                enableAll = enableAllBoolean;
            } else {
                return null;
            }

            if (!enableAll) {
                Boolean enableHeadBoolean = JsonHelper.getBoolean(parametersObject, "enableHead");
                if (Objects.nonNull(enableHeadBoolean)) {
                    enableHead = enableHeadBoolean;
                } else {
                    return null;
                }

                Boolean enableChestBoolean = JsonHelper.getBoolean(parametersObject, "enableChest");
                if (Objects.nonNull(enableChestBoolean)) {
                    enableChest = enableChestBoolean;
                } else {
                    return null;
                }

                Boolean enableLegsBoolean = JsonHelper.getBoolean(parametersObject, "enableLegs");
                if (Objects.nonNull(enableLegsBoolean)) {
                    enableLegs = enableLegsBoolean;
                } else {
                    return null;
                }

                Boolean enableFeetBoolean = JsonHelper.getBoolean(parametersObject, "enableFeet");
                if (Objects.nonNull(enableFeetBoolean)) {
                    enableFeet = enableFeetBoolean;
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }

        return this;
    }
}

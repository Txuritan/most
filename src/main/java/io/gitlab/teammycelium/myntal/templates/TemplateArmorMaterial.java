package io.gitlab.teammycelium.myntal.templates;

import com.google.gson.JsonObject;
import io.gitlab.teammycelium.myntal.JsonHelper;
import lombok.Getter;

import java.util.Objects;

public class TemplateArmorMaterial {
    @Getter
    private Integer durability;

    @Getter
    private Integer headReduction;
    @Getter
    private Integer chestReduction;
    @Getter
    private Integer legsReduction;
    @Getter
    private Integer feetReduction;

    @Getter
    private Integer enchantability;

    public TemplateArmorMaterial from(JsonObject object) {
        JsonObject parametersObject = JsonHelper.getObject(object, "parameters");
        if (Objects.nonNull(parametersObject)) {
            Integer durabilityInteger = JsonHelper.getInt(parametersObject, "durability");
            if (Objects.nonNull(durabilityInteger)) {
                durability = durabilityInteger;
            } else {
                return null;
            }

            Integer headReductionInteger = JsonHelper.getInt(parametersObject, "headReduction");
            if (Objects.nonNull(headReductionInteger)) {
                headReduction = headReductionInteger;
            } else {
                return null;
            }

            Integer chestReductionInteger = JsonHelper.getInt(parametersObject, "chestReduction");
            if (Objects.nonNull(chestReductionInteger)) {
                chestReduction = chestReductionInteger;
            } else {
                return null;
            }

            Integer legsReductionInteger = JsonHelper.getInt(parametersObject, "legsReduction");
            if (Objects.nonNull(legsReductionInteger)) {
                legsReduction = legsReductionInteger;
            } else {
                return null;
            }

            Integer feetReductionInteger = JsonHelper.getInt(parametersObject, "feetReduction");
            if (Objects.nonNull(feetReductionInteger)) {
                feetReduction = feetReductionInteger;
            } else {
                return null;
            }

            Integer enchantabilityInteger = JsonHelper.getInt(parametersObject, "enchantability");
            if (Objects.nonNull(enchantabilityInteger)) {
                enchantability = enchantabilityInteger;
            } else {
                return null;
            }
        } else {
            return null;
        }

        return this;
    }
}

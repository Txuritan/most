package io.gitlab.teammycelium.myntal.templates;

import com.google.gson.JsonObject;
import io.gitlab.teammycelium.myntal.JsonHelper;
import lombok.Getter;

import java.util.Objects;

public class TemplateTools {
    @Getter
    private TemplateToolsMaterial material;

    public TemplateTools from(JsonObject object) {
        JsonObject parametersObject = JsonHelper.getObject(object, "parameters");
        if (Objects.nonNull(parametersObject)) {
            JsonObject materialObject = JsonHelper.getObject(parametersObject, "material");
            if (Objects.nonNull(materialObject)) {
                TemplateToolsMaterial templateToolsMaterial = new TemplateToolsMaterial().from(materialObject);
                if (Objects.nonNull(templateToolsMaterial)) {
                    material = templateToolsMaterial;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }

        return this;
    }
}

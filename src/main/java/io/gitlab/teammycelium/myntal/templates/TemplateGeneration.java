package io.gitlab.teammycelium.myntal.templates;

import com.google.gson.JsonObject;
import io.gitlab.teammycelium.myntal.JsonHelper;
import io.gitlab.teammycelium.myntal.Myntal;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Objects;

public class TemplateGeneration {
    @Getter
    private Integer chance;
    @Getter
    private Integer maxHeight;
    @Getter
    private Integer minHeight;
    @Getter
    private Integer size;
    @Getter
    private Integer weight;

    @Getter
    private Boolean allBiomes = true;
    @Getter
    private ArrayList<String> biomelist = new ArrayList<>();

    @Getter
    private boolean end = false;
    @Getter
    private boolean overworld = false;
    @Getter
    private boolean nether = false;

    public TemplateGeneration from(JsonObject object) {
        String template = JsonHelper.getString(object, "template");
        if (Objects.nonNull(template)) {
            switch (template) {
                case "endGeneration":
                    end = true;
                    break;
                case "overworldGeneration":
                    overworld = true;
                    break;
                case "netherGeneration":
                    nether = true;
                    break;
            }
        }

        JsonObject parametersObject = JsonHelper.getObject(object, "parameters");
        if (Objects.nonNull(parametersObject)) {
            Integer chanceInteger = JsonHelper.getInt(parametersObject, "chance");
            if (Objects.nonNull(chanceInteger)) {
                chance = chanceInteger;
            } else {
                return null;
            }

            Integer maxHeightInteger = JsonHelper.getInt(parametersObject, "maxHeight");
            if (Objects.nonNull(maxHeightInteger)) {
                maxHeight = maxHeightInteger;
            } else {
                return null;
            }

            Integer minHeightInteger = JsonHelper.getInt(parametersObject, "minHeight");
            if (Objects.nonNull(minHeightInteger)) {
                minHeight = minHeightInteger;
            } else {
                return null;
            }

            Integer sizeInteger = JsonHelper.getInt(parametersObject, "size");
            if (Objects.nonNull(sizeInteger)) {
                size = sizeInteger;
            } else {
                return null;
            }

            Integer weightInteger = JsonHelper.getInt(parametersObject, "weight");
            if (Objects.nonNull(weightInteger)) {
                weight = weightInteger;
            } else {
                return null;
            }
        } else {
            return null;
        }

        return this;
    }
}

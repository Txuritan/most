package io.gitlab.teammycelium.myntal.templates;

import com.google.gson.JsonObject;
import io.gitlab.teammycelium.myntal.JsonHelper;
import lombok.Getter;

import java.util.Objects;

public class TemplateOre {
    @Getter
    private Float hardness;
    @Getter
    private Float resistance;
    @Getter
    private Integer maxExperience;
    @Getter
    private Integer minExperience;
    @Getter
    private Integer texture;

    @Getter
    private boolean solid = true;

    public TemplateOre from(JsonObject object) {
        String template = JsonHelper.getString(object, "template");
        if (Objects.nonNull(template)) {
            solid = template.equalsIgnoreCase("oreSolid");
        } else {
            return null;
        }

        JsonObject parametersObject = JsonHelper.getObject(object, "parameters");
        if (Objects.nonNull(parametersObject)) {
            Float hardnessFloat = JsonHelper.getFloat(parametersObject, "hardness");
            if (Objects.nonNull(hardnessFloat)) {
                hardness = hardnessFloat;
            } else {
                return null;
            }

            Float resistanceFloat = JsonHelper.getFloat(parametersObject, "resistance");
            if (Objects.nonNull(resistanceFloat)) {
                resistance = resistanceFloat;
            } else {
                return null;
            }

            Integer maxExperienceInteger = JsonHelper.getInt(parametersObject, "maxExperience");
            if (Objects.nonNull(maxExperienceInteger)) {
                maxExperience = maxExperienceInteger;
            } else {
                return null;
            }

            Integer minExperienceInteger = JsonHelper.getInt(parametersObject, "minExperience");
            if (Objects.nonNull(minExperienceInteger)) {
                minExperience = minExperienceInteger;
            } else {
                return null;
            }

            Integer textureInteger = JsonHelper.getInt(parametersObject, "texture");
            if (Objects.nonNull(textureInteger)) {
                texture = textureInteger;
            } else {
                return null;
            }
        } else {
            return null;
        }

        return this;
    }
}

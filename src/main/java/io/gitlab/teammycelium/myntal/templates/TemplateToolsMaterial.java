package io.gitlab.teammycelium.myntal.templates;

import com.google.gson.JsonObject;
import io.gitlab.teammycelium.myntal.JsonHelper;
import lombok.Getter;

import java.util.Objects;

public class TemplateToolsMaterial {
    @Getter
    private Integer durability;
    @Getter
    private Integer harvestLevel;
    @Getter
    private Float speed;
    @Getter
    private Float damage;
    @Getter
    private Integer enchantability;

    public TemplateToolsMaterial from(JsonObject object) {
        JsonObject parametersObject = JsonHelper.getObject(object, "parameters");
        if (Objects.nonNull(parametersObject)) {
            Integer durabilityInteger = JsonHelper.getInt(parametersObject, "durability");
            if (Objects.nonNull(durabilityInteger)) {
                durability = durabilityInteger;
            } else {
                return null;
            }

            Integer harvestLevelInteger = JsonHelper.getInt(parametersObject, "harvestLevel");
            if (Objects.nonNull(harvestLevelInteger)) {
                harvestLevel = harvestLevelInteger;
            } else {
                return null;
            }

            Float speedFloat = JsonHelper.getFloat(parametersObject, "speed");
            if (Objects.nonNull(speedFloat)) {
                speed = speedFloat;
            } else {
                return null;
            }

            Float damageFloat = JsonHelper.getFloat(parametersObject, "damage");
            if (Objects.nonNull(damageFloat)) {
                damage = damageFloat;
            } else {
                return null;
            }

            Integer enchantabilityInteger = JsonHelper.getInt(parametersObject, "enchantability");
            if (Objects.nonNull(enchantabilityInteger)) {
                enchantability = enchantabilityInteger;
            } else {
                return null;
            }
        } else {
            return null;
        }

        return this;
    }
}

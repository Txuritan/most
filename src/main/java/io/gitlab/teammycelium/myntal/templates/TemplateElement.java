package io.gitlab.teammycelium.myntal.templates;

import com.google.gson.JsonObject;
import io.gitlab.teammycelium.myntal.JsonHelper;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Objects;

public class TemplateElement {
    @Getter
    private TemplateElementTypes type;

    @Getter
    private String name;
    @Getter
    private String color;
    @Getter
    private String rarity;

    @Getter
    private ArrayList<String> text;

    @Getter
    private TemplateArmor armor;
    @Getter
    private TemplateGeneration generation;
    @Getter
    private TemplateOre ore;
    @Getter
    private TemplateTools tools;

    public TemplateElement from(TemplateElementTypes typ, JsonObject object) {
        type = typ;

        JsonObject parametersObject = JsonHelper.getObject(object, "parameters");
        if (Objects.nonNull(parametersObject)) {
            String nameString = JsonHelper.getString(parametersObject, "name");
            if (Objects.nonNull(nameString)) {
                name = nameString;
            } else {
                return null;
            }

            String colorString = JsonHelper.getString(parametersObject, "color");
            if (Objects.nonNull(colorString)) {
                color = colorString;
            } else {
                return null;
            }

            String rarityString = JsonHelper.getString(parametersObject, "rarity");
            if (Objects.nonNull(rarityString)) {
                rarity = rarityString;
            } else {
                return null;
            }

            ArrayList<String> textArrayString = JsonHelper.getArrayString(parametersObject, "text");
            if (Objects.nonNull(textArrayString)) {
                text = textArrayString;
            } else {
                return null;
            }

            JsonObject armorObject = JsonHelper.getObject(parametersObject, "armor");
            if (Objects.nonNull(armorObject)) {
                TemplateArmor templateArmor = new TemplateArmor().from(armorObject);
                if (Objects.nonNull(templateArmor)) {
                    armor = templateArmor;
                } else {
                    return null;
                }
            } else {
                return null;
            }

            JsonObject generationObject = JsonHelper.getObject(parametersObject, "generation");
            if (Objects.nonNull(generationObject)) {
                TemplateGeneration templateGeneration = new TemplateGeneration().from(generationObject);
                if (Objects.nonNull(templateGeneration)) {
                    generation = templateGeneration;
                } else {
                    return null;
                }
            } else {
                return null;
            }

            JsonObject oreObject = JsonHelper.getObject(parametersObject, "ore");
            if (Objects.nonNull(oreObject)) {
                TemplateOre templateOre = new TemplateOre().from(oreObject);
                if (Objects.nonNull(templateOre)) {
                    ore = templateOre;
                } else {
                    return null;
                }
            } else {
                return null;
            }

            JsonObject toolsObject = JsonHelper.getObject(parametersObject, "tools");
            if (Objects.nonNull(toolsObject)) {
                TemplateTools templateTools = new TemplateTools().from(toolsObject);
                if (Objects.nonNull(templateTools)) {
                    tools = templateTools;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }

        return this;
    }
}

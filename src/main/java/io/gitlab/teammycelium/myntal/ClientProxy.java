package io.gitlab.teammycelium.myntal;

import cpw.mods.fml.client.registry.RenderingRegistry;
import io.gitlab.teammycelium.myntal.blocks.OreBlock;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.util.ResourceLocation;

import java.lang.reflect.Method;

public class ClientProxy extends CommonProxy {
    public static int renderProxy;
    public static int render;

    public void registerRenderers() {
        for (OreBlock ore : Myntal.INSTANCE.oreBlocks) {
            // ore.renderID = RenderingRegistry.getNextAvailableRenderId();
        }
    }

    public void init() {
        Class clz = TextureMap.class;
        Method[] meths = clz.getDeclaredMethods();

        for(Method m : meths) {
            if(m.getReturnType() == ResourceLocation.class) {
                m.setAccessible(true);
                resourceLocation = m;
            }
        }
    }
}